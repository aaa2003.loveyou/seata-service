/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.server;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author spilledyear@outlook.com
 */
@SpringBootApplication

public class ServerApplication {
    private static Logger log = LoggerFactory.getLogger(ServerApplication.class);
    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();
        log.info("=========================== 啟動 Seata Service ===========================");
        log.info("args:{}", JSON.toJSONString(args));
        SpringApplication application = new SpringApplication(ServerApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        log.info("=========================== 啟動完成 耗时:{} ===========================", (System.currentTimeMillis() - startTime));
    }
}
